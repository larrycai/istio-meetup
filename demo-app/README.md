# Introduction

There are two simple static webpages docker image for demo, which I manually build and upload.

````
$ docker build -t larrycai/istio-meetup:v1 -f Dockerfile.v1 .
$ docker push larrycai/istio-meetup:v1
$ docker build -t larrycai/istio-meetup:v2 -f Dockerfile.v2 .
$ docker push larrycai/istio-meetup:v2
````

It can be run like below

````
$ docker run -it -p 5000:80 larrycai/istio-meetup:v2
````

See snapshot for v2

![](istio-meetup-v2.png)

