# Install the BookInfo app

![](https://istio.io/docs/examples/bookinfo/withistio.svg)

The sample app is [BookInfo](https://github.com/istio/istio/tree/master/samples/bookinfo)

The `istioctl` needs to inject the [sidecar](https://www.abhishek-tiwari.com/a-sidecar-for-your-service-mesh/) into the pods by using command `istioctl kube-inject`

```
kubectl create -f <(istioctl kube-inject -f samples/bookinfo/platform/kube/bookinfo.yaml)
```

Then we can check the service & pods are ready there

```
$ kubectl get services
NAME                     TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                                                               AGE
details                  ClusterIP   10.107.69.165    <none>        9080/TCP                                                              31s
kubernetes               ClusterIP   10.96.0.1        <none>        443/TCP                                                               29d
productpage              ClusterIP   10.96.141.216    <none>        9080/TCP                                                              30s
ratings                  ClusterIP   10.103.180.236   <none>        9080/TCP                                                              30s
reviews                  ClusterIP   10.111.214.56    <none>        9080/TCP
$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
details-v1-8584b997b5-v4f2b       2/2     Running   0          13m
productpage-v1-7c54588b59-2htpk   2/2     Running   0          13m
ratings-v1-857d476c65-zf7cj       2/2     Running   0          13m
reviews-v1-7746774bd6-qzk45       2/2     Running   0          13m
reviews-v2-7c687fc54c-tv7lr       2/2     Running   0          13m
reviews-v3-8565479856-mqtcs       2/2     Running   0          13m                               
```

## Access the service

By default, the service is not accessable from external, there is `istio ingress envoy` in front. but we can start small application inside to check.

````
$ kubectl run -i --rm --restart=Never dummy --image=busybox --command -- sh -c 'wget -q -O- productpage:9080/ | grep title'
    <title>Simple Bookstore App</title>
````

Now we need to create gateway with virtualservice

```
$ kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml
$ kubectl get gateway
NAME               AGE
bookinfo-gateway   28s
$ kubectl get virtualservice
NAME       GATEWAYS             HOSTS   AGE
bookinfo   [bookinfo-gateway]   [*]     38s
$ kubectl get svc istio-ingressgateway -n istio-system
NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                                                                                                                   AGE
istio-ingressgateway   LoadBalancer   10.106.175.220   <pending>     80:31380/TCP,443:31390/TCP,31400:31400/TCP,15011:30582/TCP,8060:30367/TCP,853:30507/TCP,15030:32638/TCP,15031:31140/TCP   152m
or 
$ export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
```

The port `31380` mapped to `80` is the `NodePort` ip address, inside VM, you can access with `localhost:31380/productpage`.

```
$  curl -v -o /dev/null -s -w "%{http_code}\n" localhost:31380/productpage
> GET /productpage HTTP/1.1
> Host: localhost:31380
> User-Agent: curl/7.60.0
> Accept: */*
>
< HTTP/1.1 200 OK
< content-type: text/html; charset=utf-8
< content-length: 5719
< server: envoy
< date: Tue, 12 Mar 2019 15:04:42 GMT
< x-envoy-upstream-service-time: 36
<
{ [5719 bytes data]
200
```

If you want to access from Windows 10, then find the IP address for the VM like http://192.168.99.101:31380/productpage

Or add port forwarding in virtualbox, then it could be [http://localhost:31380/productpage](http://localhost:31380/productpage)

All will bring up the page, if you reload, you will get different version of product page for review (there are 3 versions exist) since it is not controlled yet.

![](img/istio-bookinfo.png)

## Take a deep look at monitor system (grafana, service graph)

Now we can check more for the applicaiton, we need expose some NodePort (range 30000-32767)

servicegraph is replaced with [kiali](https://www.kiali.io/)

```
$ kubectl expose svc prometheus --name p2 -n istio-system --type=NodePort # 9090
$ kubectl expose svc grafana --name g2 -n istio-system --type=NodePort    # 3000
$ kubectl expose svc kiali --name k2 -n istio-system --type=NodePort # 20001
```

Or you can use `port-forward` to check one by one (don't forget to add `--address 0.0.0.0` to allow access from others

```
$ kubectl port-forward service/servicegraph -n istio-system --address 0.0.0.0 8088:8088
```

### grafana

There are several default istio dashboard inside, check one example of Istio mesh dashboard: http://192.168.99.101:3000/d/1/istio-mesh-dashboard

![](https://istio.io/docs/tasks/telemetry/using-istio-dashboard/dashboard-with-traffic.png) 

### kiali (replace servicegraph from 1.1.0)

For kiali, simulate the traffic using `watch` command

````
$ watch -n 1 curl -o /dev/null -s -w %{http_code} localhost:31380/productpage
````
you can see like http://192.168.99.101:20001/kiali/console, see more in [kiali task](https://istio.io/docs/tasks/telemetry/kiali/) 

![](img/istio-kiali.png)

## Monitor the traffic

Now we want to check how the traffic is distributed to different review version (`v1`,`v2`,`v3`)

Firstly, we run a client to randomly generate traffic  

```
export GATEWAY_URL=localhost:31380
for i in {1..100}; do curl -o /dev/null -s -w "%{http_code}\n" http://${GATEWAY_URL}/productpage; sleep $[ ( $RANDOM % 10 )  + 1 ]s; done
```

Then we monitor the istio grafana `Service Dashboard` and select service for `reviews.default.svc.cluster.local`

Nicely we got

![](img/istio-grafana.png)

## Traffic Management

[Traffic Management](https://istio.io/docs/concepts/traffic-management) is used to control your service to different version without changing the application codes, all is done in service mesh.

![](https://istio.io/docs/concepts/traffic-management/ServiceModel_Versions.svg)

Let's do a task to [Traffic Management](https://istio.io/docs/tasks/traffic-management/) to see how we redirect all traffic to one version `reviews-v2`

* [Apply default destination rules](https://istio.io/docs/examples/bookinfo/#apply-default-destination-rules)

```
$ kubectl apply -f samples/bookinfo/networking/destination-rule-all.yaml
destinationrule.networking.istio.io/productpage created
destinationrule.networking.istio.io/reviews created
destinationrule.networking.istio.io/ratings created
destinationrule.networking.istio.io/details created
```

* [Change service to all V1](https://istio.io/docs/tasks/traffic-management/request-routing/)

````
$ kubectl apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml
virtualservice.networking.istio.io/productpage created
virtualservice.networking.istio.io/reviews created
virtualservice.networking.istio.io/ratings created
virtualservice.networking.istio.io/details created
```` 

Then if you open the product webpage, it goes to v1 !!, which can be confirmed from kiali as well 

![](img/istio-kiali-2.png)

# Uninstall

Cleanup everything for book sample 

```
$ samples/bookinfo/platform/kube/cleanup.sh
$ kubectl get pods --all-namespaces
````

# Reference
* https://blog.buberel.org/2010/07/howto-random-sleep-duration-in-bash.html
* https://github.com/etiennetremel/istio-cross-namespace-canary-release-demo
* [Book:Istio in Action](https://www.manning.com/books/istio-in-action) 
* https://github.com/stefanprodan/k8s-podinfo/blob/master/docs/7-istio.md
* https://istio.io/docs/examples/bookinfo/
* [Istio 101 with Minikube](https://meteatamel.wordpress.com/2018/04/24/istio-101-with-minikube/)