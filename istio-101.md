# Introduction

[istio](http://istio.io) is one framework for service mesh (others like [linkerd](https://linkerd.io/), [consul connect](https://www.consul.io/docs/connect))

This is the tutorial of istio on minikube followed with article [Istio 101 with Minikube](https://meteatamel.wordpress.com/2018/04/24/istio-101-with-minikube/) with new env below and extra updates for easy understanding

* istio 1.1.6
* kubernetes 1.14.0 
* minikube/virtualbox 5.2.26 env (Windows 10) 
* (optional) MobaXterm

## Preparation

* Windows 10 minikube is installed in advance using virtualbox (increase to 8G memory)
* `helm` & `kubectl` are installed (Tips: I installed Linux binary in Windows Host)

# Istio 

## Install istioctl

Install istio binary directly from [releases](https://github.com/istio/istio/releases)

```
# ISTIO_VERSION=1.1.6
$ curl -L -O https://github.com/istio/istio/releases/download/1.1.6/istio-1.1.6-linux.tar.gz
$ gunzip < istio-1.1.6-linux.tar.gz | tar xvf - # windows minikube doesn't have gnu tar with -z
$ sudo cp istio-1.1.6/bin/istioctl /usr/bin # or other folder in your path
```

Now you can get the binary in path

```
$ istioctl version -s
1.1.6
```

## Install istio to k8s

Now it is time to install `istio` into k8s

```
$ kubectl apply -f install/kubernetes/istio-demo.yaml
namespace/istio-system created
..


$ kubectl get pods,svc -n istio-system
..
service/grafana                  ClusterIP      10.110.242.119   <none>        3000/TCP                                                                                                                                     2m3s
...
service/istio-ingressgateway     LoadBalancer   10.108.74.19     <pending>     15020:30551/TCP,80:31380/TCP,443:31390/TCP,31400:31400/TCP,15029:32575/TCP,15030:31149/TCP,15031:30067/TCP,15032:30777/TCP,15443:32569/TCP   2m3s
...
service/kiali                    ClusterIP      10.111.53.120    <none>        20001/TCP          
# you shall get most of them in running status and 2 or 3 are in completed status
````

If some pods are not started correct like pending, you may need to check the log and information. Which you may notice it is your memory issues especially for local environment 

```
$ kubectl describe pods istio-pilot-649455846-42hsb -n istio-system
$ kubectl logs istio-pilot-649455846-42hsb -n istio-system istio-proxy
```

## Uninstall istio in k8s

```
$ kubectl delete -f install/kubernetes/istio-demo.yaml
```

## Demo for canary deployment

### Normal installation for meetup web app v1
Deploy v1 via istio and access via NodePort (minikube need port forwarding)

````
$ kubectl apply -f istio/meetup-v1.yaml
$ kubectl apply -f istio/meetup-nodeport.yaml
$ kubectl get pods,svc
````

Then you will see the web application (v1) is up running 

### Switch to istio installation for meetup web app v1

Apply switch to using istio injection (delete old)

````
$ kubectl delete -f istio/meetup-v1.yaml
$ kubectl apply -f <(istioctl kube-inject -f istio/meetup-v1.yaml)
$ kubectl apply -f istio/meetup-destination-v1.yaml
$ kubectl apply -f istio/meetup-gateway.yaml
$ kubectl apply -f istio/meetup-service-all-v1.yaml
````

Check all the env

````
$ kubectl get gateway,virtualservice,destinationrule,svc,pods
````

Then you will see the web application (v1) is up running via ingressgateway (istio)

### deploy meetup web app v2 (canary)

Now deploy to v2 and switch traffic from firefox to new, key code

````
# meetup-service-firefox-v2.yaml
  http:
  - match:
    - headers:
        user-agent:
          regex: ".*Firefox.*"
    route:
    - destination:
        host: meetup-app
        port:
          number: 80
        subset: v2
  - route:
    - destination:
        host: meetup-app
        port:
          number: 80
        subset: v1
````

````
$ kubectl apply -f <(istioctl kube-inject -f istio/meetup-v2.yaml)
$ kubectl apply -f istio/meetup-destination-all.yaml
$ kubectl apply -f istio/meetup-service-firefox-v2.yaml
````

Now we can verify via browser or command line (`31380` is port for ingressgateway)

````
$ curl  -s localhost:31380 | grep border
$ curl  -s localhost:31380 -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0" | grep border 
````

### upgrade to v2 completely

````
$ kubectl apply -f istio/meetup-service-all-v2.yaml
# u can delete v1 after several days
````

Check all the browser client now

## Check in GUI

### grafana (port 3000)

```
$ kubectl port-forward service/grafana -n istio-system --address 0.0.0.0 3000:3000
```

Also generate traffic

````
$ watch -n 1 curl -o /dev/null -s -w %{http_code} localhost:31380
````

There are several default istio dashboard inside, check one example of Istio mesh dashboard: http://localhost:3000/d/1/istio-mesh-dashboard

![](https://istio.io/docs/tasks/telemetry/metrics/using-istio-dashboard/istio-service-dashboard.png)

### kiali (port 20001)

```
$ kubectl port-forward service/kiali -n istio-system --address 0.0.0.0 20001:20001
```

For kiali, simulate the traffic using `watch` command (`%20` traffic to v2)

````
$ watch -n 1 curl -o /dev/null -s -w %{http_code} localhost:31380
$ watch -n 4 curl -o /dev/null -A "Firefox/67.0" -s -w %{http_code} localhost:31380

````

you can check http://localhost:20001/kiali/console, see more in [kiali task](https://istio.io/docs/tasks/telemetry/kiali/) 

![](img/kiali.png)

## uninstall for the meetup app

Use `istio/clean-demo.sh` to delete service related resource, it execute below commands

````
$ kubectl delete -f istio/meetup-service-firefox-v2.yaml
$ kubectl delete -f istio/meetup-destination-all.yaml
$ kubectl delete -f istio/meetup-gateway.yaml
$ kubectl delete -f istio/meetup-nodeport.yaml
$ kubectl delete -f <(istioctl kube-inject -f istio/meetup-v1.yaml)
$ kubectl delete -f <(istioctl kube-inject -f istio/meetup-v2.yaml)
````

## Debug command

````
$ istioctl proxy-config cluster meetup-app-v1-58c4d666cc-qvpc2
````

# FAQ

## istio-1.1.9 issue for demo

`istio-1.1.9` doesn't work well for extra warning and grafana/kiali GUI, so use `1.1.6` instead

````
# during install istio
unable to recognize "install/kubernetes/istio-demo.yaml": no matches for kind "attributemanifest" in version "config.istio.io/v1alpha2"
..
````

## Minikube Mac env

Anyone contribute `istio-101-mac.md`?

# Reference
* https://www.whatismybrowser.com/detect/what-is-my-user-agent