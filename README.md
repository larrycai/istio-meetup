# Introduction

It contains the sample codes and all the materials for my presentation `Get started Service Mesh with Kubernetes and Istio` in [Stockholm Hadoop User group Meetup](https://www.meetup.com/stockholm-hug/events/262285559/) in June 27, 2019

* slides: [Get Started Service Mesh with Kubernetes and Istio@slideshare](https://www.slideshare.net/larrycai/get-started-service-mesh-with-kubernetes-and-istio)
* demo detail: [istio-101.md](istio-101.md) canary deployment using minikube/virtualbox@windows

## Snapshots

* canary deploy `meetup web app` from v1 to v2 (see [demo-app](demo-app) for app)

![upgrade v1 to v2](img/meetup-demo-v1-v2.png)

* use istio's kiali to track the traffic flow

![](img/kiali.png)

# Other possible demo

* [bookinfo.md](bookinfo.md) is official demo everywhere

# Reference

* [Book: Istio in Action](https://www.manning.com/books/istio-in-action) 
* [Istio 101 with Minikube](https://meteatamel.wordpress.com/2018/04/24/istio-101-with-minikube/)



