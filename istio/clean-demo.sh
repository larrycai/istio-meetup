kubectl delete -f istio/meetup-service-firefox-v2.yaml
kubectl delete -f istio/meetup-destination-all.yaml
kubectl delete -f istio/meetup-gateway.yaml
kubectl delete -f istio/meetup-nodeport.yaml
kubectl delete -f <(istioctl kube-inject -f istio/meetup-v1.yaml)
kubectl delete -f <(istioctl kube-inject -f istio/meetup-v2.yaml)